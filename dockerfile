FROM golang:1.10-alpine

RUN mkdir /app

ADD . /app/
WORKDIR /app

RUN go build -i /app/src/main.go

CMD ["/app/main"]
